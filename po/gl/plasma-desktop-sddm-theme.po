# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015, 2016.
# SPDX-FileCopyrightText: 2023, 2024 Adrián Chaves (Gallaecio)
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-22 00:40+0000\n"
"PO-Revision-Date: 2024-02-04 12:54+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Proxecto Trasno (proxecto@trasno.gal)\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: KeyboardButton.qml:19
msgid "Keyboard Layout: %1"
msgstr "Disposición do teclado: %1"

#: Login.qml:85
msgid "Username"
msgstr "Nome de usuaria"

#: Login.qml:102
msgid "Password"
msgstr "Contrasinal"

#: Login.qml:144 Login.qml:150
msgid "Log In"
msgstr "Acceder"

#: Main.qml:195
msgid "Caps Lock is on"
msgstr "As maiúsculas están bloqueadas"

#: Main.qml:207 Main.qml:351
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Durmir"

#: Main.qml:214 Main.qml:358
msgid "Restart"
msgstr "Reiniciar"

#: Main.qml:221 Main.qml:365
msgid "Shut Down"
msgstr "Apagar"

#: Main.qml:228
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr "Máis…"

#: Main.qml:337
msgid "Type in Username and Password"
msgstr "Escriba o usuario e o contrasinal"

#: Main.qml:372
msgid "List Users"
msgstr "Listar os usuarios"

#: Main.qml:447
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "Teclado virtual"

#: Main.qml:514
msgid "Login Failed"
msgstr "Fallou o acceso"

#: SessionButton.qml:18
msgid "Desktop Session: %1"
msgstr "Sesión de escritorio: %1"
