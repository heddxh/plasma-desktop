# Danish translation of kcmsmserver
# Copyright (C).
#
# Erik Kjær Pedersen <erik@binghamton.edu>, 2000, 2002, 2003, 2004, 2005.
# Martin Schlander <mschlander@opensuse.org>, 2008, 2009, 2019, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcmsmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-25 00:40+0000\n"
"PO-Revision-Date: 2022-08-20 17:45+0200\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. i18n: ectx: label, entry (confirmLogout), group (General)
#: smserversettings.kcfg:9
#, kde-format
msgid "Confirm logout"
msgstr "Bekræft når der logges ud"

#. i18n: ectx: label, entry (shutdownType), group (General)
#: smserversettings.kcfg:13
#, kde-format
msgid "Default leave option"
msgstr "Standardvalg for Forlad"

#. i18n: ectx: label, entry (loginMode), group (General)
#: smserversettings.kcfg:22
#, kde-format
msgid "On login"
msgstr "Ved login"

#. i18n: ectx: label, entry (excludeApps), group (General)
#: smserversettings.kcfg:26
#, kde-format
msgid "Applications to be excluded from session"
msgstr "Programmer der skal udelukkes fra session"

#: ui/main.qml:31
#, kde-format
msgid ""
"The system must be restarted before manual session saving becomes active."
msgstr ""

#: ui/main.qml:36
#, fuzzy, kde-format
#| msgid "Restart Now"
msgid "Restart"
msgstr "Genstart nu"

#: ui/main.qml:58
#, kde-format
msgid "Failed to request restart to firmware setup: %1"
msgstr "Kunne ikke anmode genstart af firmware-opsætning: %1"

#: ui/main.qml:59
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the UEFI setup screen."
msgstr ""
"Computeren vil gå ind i UEFI-opsætningsskærmen næste gang den genstartes."

#: ui/main.qml:60
#, kde-format
msgid ""
"Next time the computer is restarted, it will enter the firmware setup screen."
msgstr ""
"Computeren vil gå ind i firmware-opsætningsskærmen næste gang den genstartes."

#: ui/main.qml:65
#, kde-format
msgid "Restart Now"
msgstr "Genstart nu"

#: ui/main.qml:74
#, fuzzy, kde-format
#| msgid "General:"
msgctxt "@title:group"
msgid "General"
msgstr "Generelt:"

#: ui/main.qml:79
#, kde-format
msgctxt ""
"@label beginning of the logical sentence 'Ask for confirmation on shutdown, "
"restart, and logout.'"
msgid "Ask for confirmation:"
msgstr ""

#: ui/main.qml:80
#, kde-format
msgctxt ""
"@option:check end of the logical sentence 'Ask for confirmation on shutdown, "
"restart, and logout.'"
msgid "On shutdown, restart, and logout"
msgstr ""

#: ui/main.qml:91
#, fuzzy, kde-format
#| msgid "Session Manager"
msgctxt "@title:group"
msgid "Session Restore"
msgstr "Sessionshåndtering"

#: ui/main.qml:99
#, kde-format
msgid "On login, launch apps that were open:"
msgstr ""

#: ui/main.qml:100
#, kde-format
msgctxt "@option:radio Automatic style of session restoration"
msgid "On last logout"
msgstr ""

#: ui/main.qml:113
#, fuzzy, kde-format
#| msgctxt ""
#| "@option:radio Here 'session' refers to the technical concept of session "
#| "restoration, whereby the windows that were open on logout are re-opened "
#| "on the next login"
#| msgid "Restore last manually saved session"
msgctxt "@option:radio Manual style of session restoration"
msgid "When session was manually saved"
msgstr "Genskab seneste manuelt gemt session"

#: ui/main.qml:122
#, kde-kuit-format
msgctxt "@info"
msgid ""
"A <interface>Save Session</interface> button will appear in the "
"<interface>Application Launcher</interface> menu. When you click it, Plasma "
"will remember the apps that are open and restore them on the next login. "
"Click it again to replace the set of remembered apps."
msgstr ""

#: ui/main.qml:127
#, kde-format
msgctxt ""
"@option:radio Here 'session' refers to the technical concept of session "
"restoration, whereby the windows that were open on logout are re-opened on "
"the next login"
msgid "Start with an empty session"
msgstr "Start med en tom session"

#: ui/main.qml:141
#, fuzzy, kde-format
#| msgid "Don't restore these applications:"
msgid "Ignored applications:"
msgstr "Genskab ikke disse programmer:"

#: ui/main.qml:164
#, kde-format
msgid ""
"Write apps' executable names here (separated by commas or colons, for "
"example 'xterm:konsole' or 'xterm,konsole') to prevent them from "
"autostarting along with other session-restored apps."
msgstr ""

#: ui/main.qml:171
#, fuzzy, kde-format
#| msgid "Firmware Setup"
msgctxt "@title:group"
msgid "Firmware"
msgstr "Opsætning af firmware"

#: ui/main.qml:177
#, kde-format
msgctxt ""
"@label:check part of a sentence: After next restart enter UEFI/Firmware "
"setup screen"
msgid "After next restart:"
msgstr ""

#: ui/main.qml:178
#, fuzzy, kde-format
#| msgid "Enter UEFI setup on next restart"
msgctxt "@option:check"
msgid "Enter UEFI setup screen"
msgstr "Gå ind i UEFI-opsætningen ved næste genstart"

#: ui/main.qml:179
#, fuzzy, kde-format
#| msgctxt "@option:check"
#| msgid "Enter firmware setup screen on next restart"
msgctxt "@option:check"
msgid "Enter firmware setup screen"
msgstr "Gå ind i firmware-opsætningen ved næste genstart"

#~ msgid ""
#~ "<h1>Session Manager</h1> You can configure the session manager here. This "
#~ "includes options such as whether or not the session exit (logout) should "
#~ "be confirmed, whether the session should be restored again when logging "
#~ "in and whether the computer should be automatically shut down after "
#~ "session exit by default."
#~ msgstr ""
#~ "<h1>Sessionshåndtering</h1> Her kan du indstille sessionshåndteringen. "
#~ "Dette inkluderer valg, såsom hvorvidt du vil bekræfte, at du vil logge "
#~ "af, om den sidste session skal genskabes når du logger på igen, og om "
#~ "computeren skal lukke ned automatisk efter sessionsafslutning som "
#~ "standard."

#, fuzzy
#~| msgid "Default leave option:"
#~ msgid "Default option:"
#~ msgstr "Standardvalg for Forlad:"

#, fuzzy
#~| msgid "Restart Now"
#~ msgctxt "@option:radio"
#~ msgid "Restart"
#~ msgstr "Genstart nu"

#~ msgctxt "@option:check"
#~ msgid "Confirm logout"
#~ msgstr "Bekræft når der logges ud"

#~ msgctxt ""
#~ "@option:radio Here 'session' refers to the technical concept of session "
#~ "restoration, whereby the windows that were open on logout are re-opened "
#~ "on the next login"
#~ msgid "End current session"
#~ msgstr "Afslut denne session"

#~ msgctxt "@option:radio"
#~ msgid "Restart computer"
#~ msgstr "Genstart computeren"

#~ msgctxt "@option:radio"
#~ msgid "Turn off computer"
#~ msgstr "Sluk computeren"

#~ msgid "When logging in:"
#~ msgstr "Ved log ind:"

#~ msgctxt ""
#~ "@option:radio Here 'session' refers to the technical concept of session "
#~ "restoration, whereby the windows that were open on logout are re-opened "
#~ "on the next login"
#~ msgid "Restore last session"
#~ msgstr "Genskab seneste session"

#~ msgid ""
#~ "Here you can enter a colon or comma separated list of applications that "
#~ "should not be saved in sessions, and therefore will not be started when "
#~ "restoring a session. For example 'xterm:konsole' or 'xterm,konsole'."
#~ msgstr ""
#~ "Her kan du indtaste en kolon- eller komma-adskilt liste over programmer "
#~ "der ikke skal gemmes i sessioner, og derfor ikke vil blive startet når "
#~ "sessionen genskabes. For eksempel \"xterm:xconsole\" eller \"xterm,konsole"
#~ "\"."

#~ msgctxt "@option:check"
#~ msgid "Offer shutdown options"
#~ msgstr "Tilbyd nedlukningsmuligheder"

#~ msgid "Offer shutdown options"
#~ msgstr "Tilbyd nedlukningsmuligheder"

#, fuzzy
#~| msgid "Restore &previous session"
#~ msgid "Desktop Session"
#~ msgstr "Genskab &forrige session"

#, fuzzy
#~| msgid "Restore &manually saved session"
#~ msgid "Restore previous saved session"
#~ msgstr "Genskab &manuelt gemt session"

#~ msgid "UEFI Setup"
#~ msgstr "UEFI-opsætning"

#~ msgid ""
#~ "Check this option if you want the session manager to display a logout "
#~ "confirmation dialog box."
#~ msgstr ""
#~ "Markér denne indstilling, hvis du ønsker at sessionshåndteringen skal "
#~ "vise en log ud-bekræftelsesdialog."

#~ msgid "Conf&irm logout"
#~ msgstr "Bekræft når der logges &ud"

#~ msgid "O&ffer shutdown options"
#~ msgstr "&Tilbyd nedlukningsmuligheder"

#~ msgid ""
#~ "Here you can choose what should happen by default when you log out. This "
#~ "only has meaning, if you logged in through KDM."
#~ msgstr ""
#~ "Her kan du vælge hvad der skal ske som standard når du logger ud. Dette "
#~ "har kun betydning hvis du er logget ind gennem KDM."

#~ msgid "Default Leave Option"
#~ msgstr "Standardvalg for \"Forlad\""

#~ msgid ""
#~ "<ul>\n"
#~ "<li><b>Restore previous session:</b> Will save all applications running "
#~ "on exit and restore them when they next start up</li>\n"
#~ "<li><b>Restore manually saved session: </b> Allows the session to be "
#~ "saved at any time via \"Save Session\" in the K-Menu. This means the "
#~ "currently started applications will reappear when they next start up.</"
#~ "li>\n"
#~ "<li><b>Start with an empty session:</b> Do not save anything. Will come "
#~ "up with an empty desktop on next start.</li>\n"
#~ "</ul>"
#~ msgstr ""
#~ "<ul>\n"
#~ "<li><b>Genskab forrige session:</b> Vil gemme alle programmer der kører "
#~ "ved afslutning og genskabe dem ved næste opstart</li>\n"
#~ "<li><b>Genskab manuelt gemt session: </b> Lader sessionen blive gemtnår "
#~ "som helst via \"Gem session\" i K-Menuen. Dette betyder at de programmer "
#~ "der kører på dette tidspunkt vil komme tilbage ved næste opstart.</li>\n"
#~ "<li><b>Start med en tom session:</b> Gem intet. Vil komme op med et "
#~ "tomtskrivebord ved næste opstart.</li>\n"
#~ "</ul>"

#~ msgid "On Login"
#~ msgstr "Når der logges ind"

#~ msgid "Applications to be e&xcluded from sessions:"
#~ msgstr "Programmer der skal &udelukkes fra sessioner:"

#~ msgid ""
#~ "When the computer is restarted the next time, enter firmware setup screen "
#~ "(e.g. UEFI or BIOS setup)"
#~ msgstr ""
#~ "Gå ind i firmware-opsætning (f.eks. UEFI- eller BIOS-opsætning) når "
#~ "computeren genstartes næste gang"
