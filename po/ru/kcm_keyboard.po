# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Nick Shaforostoff <shaforostoff@kde.ru>, 2007.
# Artem Sereda <overmind88@gmail.com>, 2008.
# Andrey Cherepanov <skull@kde.ru>, 2009, 2011.
# Nick Shaforostoff <shafff@ukr.net>, 2010, 2011.
# Alexander Potashev <aspotashev@gmail.com>, 2010, 2011, 2012, 2014, 2015, 2016, 2017, 2018.
# Yuri Efremov <yur.arh@gmail.com>, 2013.
# SPDX-FileCopyrightText: 2020, 2021, 2023, 2024 Alexander Yavorsky <kekcuha@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: kcmkeyboard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-11 00:40+0000\n"
"PO-Revision-Date: 2024-05-06 09:12+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 24.02.0\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Николай Шафоростов"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "shafff@ukr.net"

#: bindings.cpp:24
#, kde-format
msgid "Keyboard Layout Switcher"
msgstr "Переключение раскладки клавиатуры"

#: bindings.cpp:26
#, kde-format
msgid "Switch to Next Keyboard Layout"
msgstr "Выбрать следующую раскладку клавиатуры"

#: bindings.cpp:30
#, kde-format
msgid "Switch to Last-Used Keyboard Layout"
msgstr "Выбрать последнюю использованную раскладку клавиатуры"

#: bindings.cpp:60
#, kde-format
msgid "Switch keyboard layout to %1"
msgstr "Сменить раскладку клавиатуры на %1"

#: keyboardmodel.cpp:35
#, kde-format
msgctxt "unknown keyboard model vendor"
msgid "Unknown"
msgstr "Неизвестный производитель"

#: keyboardmodel.cpp:39
#, kde-format
msgctxt "vendor | keyboard model"
msgid "%1 | %2"
msgstr "%1 | %2"

#: tastenbrett/main.cpp:52
#, kde-format
msgctxt "app display name"
msgid "Keyboard Preview"
msgstr "Просмотр раскладки клавиатуры"

#: tastenbrett/main.cpp:54
#, kde-format
msgctxt "app description"
msgid "Keyboard layout visualization"
msgstr "Просмотр раскладки клавиатуры"

#: tastenbrett/main.cpp:139
#, fuzzy, kde-format
#| msgctxt "@label"
#| msgid ""
#| "The keyboard geometry failed to load. This often indicates that the "
#| "selected model does not support a specific layout or layout variant. This "
#| "problem will likely also present when you try to use this combination of "
#| "model, layout and variant."
msgctxt "@label"
msgid ""
"The keyboard geometry failed to load. This often indicates that the selected "
"model does not support a specific layout or layout variant. This problem "
"will likely also present when you try to use this combination of model, "
"layout and variant.\n"
"Previewing layouts that are defined outside your systems xkb directory is "
"not supported and  will also trigger this message. These might still work "
"fine if applied"
msgstr ""
"Не удалось загрузить конфигурацию клавиатуры. Эта ошибка, обычно, происходит "
"при попытке использовать неподдерживаемую раскладку или вариант раскладки "
"клавиатуры. Ошибка, скорее всего, будет присутствовать при попытке "
"использовать это сочетание модели, раскладки и варианта раскладки клавиатуры."

#: ui/Advanced.qml:40
#, kde-format
msgctxt "@option:checkbox"
msgid "Configure keyboard options"
msgstr "Настроить дополнительные параметры клавиатуры"

#: ui/Hardware.qml:34
#, kde-format
msgctxt "@title:group"
msgid "Keyboard model:"
msgstr "Модель клавиатуры:"

#: ui/Hardware.qml:68
#, kde-format
msgctxt "@title:group"
msgid "NumLock on Plasma Startup:"
msgstr "Режим NumLock при запуске Plasma"

#: ui/Hardware.qml:73
#, kde-format
msgctxt "@option:radio"
msgid "Turn On"
msgstr "Включить"

#: ui/Hardware.qml:77
#, kde-format
msgctxt "@option:radio"
msgid "Turn Off"
msgstr "Отключить"

#: ui/Hardware.qml:81
#, kde-format
msgctxt "@option:radio"
msgid "Leave unchanged"
msgstr "Не изменять"

#: ui/Hardware.qml:114
#, kde-format
msgctxt "@title:group"
msgid "When key is held:"
msgstr "Действие при удержании клавиши:"

#: ui/Hardware.qml:119
#, kde-format
msgctxt "@option:radio"
msgid "Repeat the key"
msgstr "Повторять ввод символа"

#: ui/Hardware.qml:124
#, kde-format
msgctxt "@option:radio"
msgid "Do nothing"
msgstr "Ничего не делать"

#: ui/Hardware.qml:129
#, kde-format
msgctxt "@option:radio"
msgid "Show accented and similar characters"
msgstr "Показывать акцентированные и схожие символы"

#: ui/Hardware.qml:164
#, kde-format
msgctxt "@label:slider"
msgid "Delay:"
msgstr "Задержка:"

#: ui/Hardware.qml:199
#, kde-format
msgctxt "@label:slider"
msgid "Rate:"
msgstr "Частота:"

#: ui/Hardware.qml:247
#, kde-format
msgctxt "@label:textbox"
msgid "Test area:"
msgstr "Проверка:"

#: ui/Hardware.qml:248
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to test settings"
msgstr "Начните печатать для проверки настроек"

#: ui/LayoutDialog.qml:21
#, kde-format
msgctxt "@title:window"
msgid "Add Layout"
msgstr "Добавить раскладку"

#: ui/LayoutDialog.qml:106 ui/Layouts.qml:245
#, kde-format
msgctxt "@action:button"
msgid "Preview"
msgstr "Просмотр"

#: ui/LayoutDialog.qml:124
#, kde-format
msgctxt "@label:textbox"
msgid "Display text:"
msgstr ""

#: ui/LayoutDialog.qml:143
#, kde-format
msgctxt "@option:textbox"
msgid "Shortcut:"
msgstr "Комбинация клавиш:"

#: ui/Layouts.qml:28
#, kde-format
msgctxt "@title:group"
msgid "Shortcuts for Switching Layout"
msgstr "Комбинации клавиш для переключения"

#: ui/Layouts.qml:34
#, kde-format
msgctxt "@option:textbox"
msgid "Main shortcuts:"
msgstr "Основные:"

#: ui/Layouts.qml:61
#, kde-format
msgctxt "@option:textbox"
msgid "3rd level shortcuts:"
msgstr "«3-й» уровень (набор типографских символов):"

#: ui/Layouts.qml:88
#, kde-format
msgctxt "@option:textbox"
msgid "Alternative shortcut:"
msgstr "Альтернативная комбинация:"

#: ui/Layouts.qml:109
#, kde-format
msgctxt "@option:textbox"
msgid "Last used shortcuts:"
msgstr "Последняя использованная комбинация:"

#: ui/Layouts.qml:129
#, kde-format
msgctxt "@option:checkbox"
msgid "Show a popup on layout changes"
msgstr "Показывать экранные уведомления при переключении раскладки клавиатуры"

#: ui/Layouts.qml:145
#, kde-format
msgctxt "@title:group"
msgid "Switching Policy"
msgstr "Область переключения раскладки"

#: ui/Layouts.qml:152
#, kde-format
msgctxt "@option:radio"
msgid "Global"
msgstr "Глобально"

#: ui/Layouts.qml:156
#, kde-format
msgctxt "@option:radio"
msgid "Desktop"
msgstr "Рабочий стол"

#: ui/Layouts.qml:160
#, kde-format
msgctxt "@option:radio"
msgid "Application"
msgstr "Приложение"

#: ui/Layouts.qml:164
#, kde-format
msgctxt "@option:radio"
msgid "Window"
msgstr "Окно"

#: ui/Layouts.qml:185
#, kde-format
msgctxt "@option:checkbox"
msgid "Configure Layouts"
msgstr "Настроить раскладки"

#: ui/Layouts.qml:200
#, kde-format
msgctxt "@action:button"
msgid "Add"
msgstr "Добавить"

#: ui/Layouts.qml:206
#, kde-format
msgctxt "@action:button"
msgid "Remove"
msgstr "Удалить"

#: ui/Layouts.qml:213
#, kde-format
msgctxt "@action:button"
msgid "Move Up"
msgstr "Переместить вверх"

#: ui/Layouts.qml:229
#, kde-format
msgctxt "@action:button"
msgid "Move Down"
msgstr "Переместить вниз"

#: ui/Layouts.qml:316
#, kde-format
msgctxt "@title:column"
msgid "Map"
msgstr "Название"

#: ui/Layouts.qml:323
#, kde-format
msgctxt "@title:column"
msgid "Label"
msgstr "Значок"

#: ui/Layouts.qml:332
#, kde-format
msgctxt "@title:column"
msgid "Layout"
msgstr "Раскладка"

#: ui/Layouts.qml:337
#, kde-format
msgctxt "@title:column"
msgid "Variant"
msgstr "Вариант"

#: ui/Layouts.qml:342
#, kde-format
msgctxt "@title:column"
msgid "Shortcut"
msgstr "Комбинация клавиш"

#: ui/Layouts.qml:388
#, kde-format
msgctxt "@action:button"
msgid "Reassign shortcut"
msgstr "Переназначить"

#: ui/Layouts.qml:404
#, kde-format
msgctxt "@action:button"
msgid "Cancel assignment"
msgstr "Отменить"

#: ui/Layouts.qml:417
#, kde-format
msgctxt "@option:checkbox"
msgid "Spare layouts"
msgstr "Ограничить число раскладок, входящих в кольцо переключения"

#: ui/Layouts.qml:453
#, kde-format
msgctxt "@label:spinbox"
msgid "Main layout count:"
msgstr "Количество часто используемых раскладок:"

#: ui/main.qml:43
#, kde-format
msgctxt "@title:tab"
msgid "Hardware"
msgstr "Оборудование"

#: ui/main.qml:49
#, kde-format
msgctxt "@title:tab"
msgid "Layouts"
msgstr "Раскладки"

#: ui/main.qml:55
#, kde-format
msgctxt "@title:tab"
msgid "Key Bindings"
msgstr "Комбинации клавиш"

#: xkboptionsmodel.cpp:160
#, kde-format
msgid "%1 shortcut"
msgid_plural "%1 shortcuts"
msgstr[0] "%1 комбинация клавиш"
msgstr[1] "%1 комбинации клавиш"
msgstr[2] "%1 комбинаций клавиш"
msgstr[3] "%1 комбинация клавиш"

#: xkboptionsmodel.cpp:163
#, kde-format
msgctxt "no shortcuts defined"
msgid "None"
msgstr "Нет"

#~ msgctxt "layout - variant"
#~ msgid "%1 - %2"
#~ msgstr "%1 (%2)"

#~ msgid "Search…"
#~ msgstr "Поиск…"

# BUGME: значок? подпись на значке? --aspotashev
#~ msgid "Label:"
#~ msgstr "Подпись:"

#~ msgid ""
#~ "Here you can choose a keyboard model. This setting is independent of your "
#~ "keyboard layout and refers to the \"hardware\" model, i.e. the way your "
#~ "keyboard is manufactured. Modern keyboards that come with your computer "
#~ "usually have two extra keys and are referred to as \"104-key\" models, "
#~ "which is probably what you want if you do not know what kind of keyboard "
#~ "you have.\n"
#~ msgstr ""
#~ "Здесь можно указать модель клавиатуры. Этот параметр не зависит от "
#~ "раскладки клавиатуры и относится к аппаратной модели, то есть к тому, кем "
#~ "была произведена ваша клавиатура. Современные клавиатуры с двумя "
#~ "дополнительными клавишами являются 104-клавишными, скорее всего у вас "
#~ "именно такой тип клавиатуры.\n"

#~ msgid ""
#~ "If you select \"Application\" or \"Window\" switching policy, changing "
#~ "the keyboard layout will only affect the current application or window."
#~ msgstr ""
#~ "Если вы выбрали область переключения «Приложение» или «Окно», каждое "
#~ "приложение/окно будет иметь свою текущую раскладку (как в Windows)."

#~ msgid ""
#~ "This is a shortcut for switching layouts which is handled by X.org. It "
#~ "allows modifier-only shortcuts."
#~ msgstr ""
#~ "Комбинация клавиш, используемая для переключения раскладки клавиатуры, "
#~ "обрабатываемая графической системой X.org. Можно указывать только клавиши-"
#~ "модификаторы (например, Alt+Shift)."

#~ msgctxt "no shortcut defined"
#~ msgid "None"
#~ msgstr "Нет"

#~ msgid "…"
#~ msgstr "..."

#~ msgid ""
#~ "This is a shortcut for switching to a third level of the active layout "
#~ "(if it has one) which is handled by X.org. It allows modifier-only "
#~ "shortcuts."
#~ msgstr ""
#~ "Комбинация клавиш, используемая для переключения на третий уровень "
#~ "раскладки, обрабатываемая графической системой X.org. Можно указывать "
#~ "только клавиши-модификаторы (например, Alt+Shift). Для ввода типографских "
#~ "символов («, », —, ©, §, °, £, …) включите соответствующий пункт на "
#~ "вкладке «Дополнительно», в разделе «Разные параметры совместимости»."

#~ msgid ""
#~ "This is a shortcut for switching layouts. It does not support modifier-"
#~ "only shortcuts and also may not work in some situations (e.g. if popup is "
#~ "active or from screensaver)."
#~ msgstr ""
#~ "Комбинация клавиш для переключения раскладки. Она не может состоять "
#~ "исключительно из клавиш-модификаторов (например, Alt+Shift) и она может "
#~ "не работать в некоторых случаях (например, в выпадающем меню из хранителя "
#~ "экрана)."

#~ msgid ""
#~ "This shortcut allows for fast switching between two layouts, by always "
#~ "switching to the last-used one."
#~ msgstr ""
#~ "Эта комбинация клавиш позволяет последовательно переключаться между двумя "
#~ "последними использованными раскладками."

#~ msgid "Advanced"
#~ msgstr "Дополнительно"

#~ msgctxt "variant"
#~ msgid "Default"
#~ msgstr "Вариант по умолчанию"

#~ msgid ""
#~ "Allows to test keyboard repeat and click volume (just don't forget to "
#~ "apply the changes)."
#~ msgstr ""
#~ "Здесь можно проверить параметры автоповтора и громкость\n"
#~ "звука нажатия (после этого не забудьте применить изменения)."

#~ msgid ""
#~ "If supported, this option allows you to setup the state of NumLock after "
#~ "Plasma startup.<p>You can configure NumLock to be turned on or off, or "
#~ "configure Plasma not to set NumLock state."
#~ msgstr ""
#~ "Если поддерживается, этот параметр позволяет установить состояние NumLock "
#~ "после запуска Plasma. <p> Можно настроить включение или отключение "
#~ "NumLock, или не изменять состояние NumLock при запуске Plasma."

#~ msgid "Leave unchan&ged"
#~ msgstr "&Не изменять"

#~ msgid ""
#~ "If supported, this option allows you to set the delay after which a "
#~ "pressed key will start generating keycodes. The 'Repeat rate' option "
#~ "controls the frequency of these keycodes."
#~ msgstr ""
#~ "Если поддерживается, этот параметр позволяет установить задержку, после "
#~ "которой нажатая клавиша начинает генерировать коды. Параметр «Частота» "
#~ "определяет частоту автоповтора."

#~ msgid ""
#~ "If supported, this option allows you to set the rate at which keycodes "
#~ "are generated while a key is pressed."
#~ msgstr ""
#~ "Если поддерживается, этот параметр позволяет установить частоту "
#~ "автоповтора при длительном нажатии клавиши."

#~ msgid " repeats/s"
#~ msgstr " повторов/с"

#~ msgid " ms"
#~ msgstr " мс"

#~ msgid "KDE Keyboard Control Module"
#~ msgstr "Модуль настройки клавиатуры"

#~ msgid "(c) 2010 Andriy Rysin"
#~ msgstr "© Andriy Rysin, 2010"

#~ msgid ""
#~ "<h1>Keyboard</h1> This control module can be used to configure keyboard "
#~ "parameters and layouts."
#~ msgstr ""
#~ "<h1>Клавиатура</h1> Этот модуль настройки позволяет выбрать параметры и "
#~ "раскладки клавиатуры."

#~ msgid "KDE Keyboard Layout Switcher"
#~ msgstr "Переключение раскладки клавиатуры в KDE"

#~ msgid "Only up to %1 keyboard layout is supported"
#~ msgid_plural "Only up to %1 keyboard layouts are supported"
#~ msgstr[0] "Поддерживается использование не более %1 раскладки клавиатуры"
#~ msgstr[1] "Поддерживается использование не более %1 раскладок клавиатуры"
#~ msgstr[2] "Поддерживается использование не более %1 раскладок клавиатуры"
#~ msgstr[3] "Поддерживается использование не более %1 раскладки клавиатуры"

#~ msgid "Any language"
#~ msgstr "любой язык"

#~ msgid "Layout:"
#~ msgstr "Раскладка:"

#~ msgid "Variant:"
#~ msgstr "Вариант:"

#~ msgid "Limit selection by language:"
#~ msgstr "Ограничить выбор языком:"

#~ msgid "..."
#~ msgstr "..."

#~ msgctxt "short layout label - full layout name"
#~ msgid "%1 - %2"
#~ msgstr "%1 — %2"

#~ msgid "Layout Indicator"
#~ msgstr "Индикатор раскладки"

#~ msgid "Show layout indicator"
#~ msgstr "Показывать индикатор раскладки"

#~ msgid "Show for single layout"
#~ msgstr "Показывать даже для одной раскладки"

#~ msgid "Show flag"
#~ msgstr "Флаг страны"

#~ msgid "Show label"
#~ msgstr "Код языка"

#~ msgid "Show label on flag"
#~ msgstr "Код языка на флаге"

#~ msgctxt "tooltip title"
#~ msgid "Keyboard Layout"
#~ msgstr "Раскладка клавиатуры"

#~ msgid "Configure Layouts..."
#~ msgstr "Настроить раскладки клавиатуры..."

#~ msgid "Keyboard Repeat"
#~ msgstr "Автоповтор клавиш при запуске KDE"

#~ msgid "Turn o&ff"
#~ msgstr "От&ключить"

# BUGME: change to "configure layouts" --aspotashev
#~ msgid "Configure..."
#~ msgstr "Настроить раскладки..."

#~ msgid "Key Click"
#~ msgstr "Звук нажатия клавиш"

#~ msgid ""
#~ "If supported, this option allows you to hear audible clicks from your "
#~ "computer's speakers when you press the keys on your keyboard. This might "
#~ "be useful if your keyboard does not have mechanical keys, or if the sound "
#~ "that the keys make is very soft.<p>You can change the loudness of the key "
#~ "click feedback by dragging the slider button or by clicking the up/down "
#~ "arrows on the spin box. Setting the volume to 0% turns off the key click."
#~ msgstr ""
#~ "Если поддерживается, этот параметр позволяет слышать щелчки из динамиков "
#~ "компьютера при нажатии клавиш на клавиатуре. это может быть полезным, "
#~ "если на клавиатуре нет механических клавиш, или если звук от нажатия "
#~ "клавиш слишком тихий.<p>Вы можете изменить громкость щелчков, перемещая "
#~ "ползунок или при помощи стрелок на поле со счётчиком. Установка громкости "
#~ "на 0 отключает щелчки клавиатуры."

#, fuzzy
#~| msgid "Key click &volume:"
#~ msgid "&Key click volume:"
#~ msgstr "&Громкость нажатия клавиш:"

#~ msgid "No layout selected "
#~ msgstr "Не выбрана раскладка"

#~ msgid "XKB extension failed to initialize"
#~ msgstr "Не удалось инициализировать расширение XKB"

#~ msgid "Backspace"
#~ msgstr "Backspace"

#~ msgctxt "Tab key"
#~ msgid "Tab"
#~ msgstr "Tab"

#~ msgid "Caps Lock"
#~ msgstr "Caps Lock"

#~ msgid "Enter"
#~ msgstr "Enter"

#~ msgid "Ctrl"
#~ msgstr "Ctrl"

#~ msgid "Alt"
#~ msgstr "Alt"

#~ msgid "AltGr"
#~ msgstr "AltGr"

#~ msgid "Esc"
#~ msgstr "Esc"

#~ msgctxt "Function key"
#~ msgid "F%1"
#~ msgstr "F%1"

#~ msgid "Shift"
#~ msgstr "Shift"

#~ msgid "No preview found"
#~ msgstr "Изображение предварительного просмотра отсутствует"

#~ msgid "Close"
#~ msgstr "Закрыть"

#~ msgid ""
#~ "If you check this option, pressing and holding down a key emits the same "
#~ "character over and over again. For example, pressing and holding down the "
#~ "Tab key will have the same effect as that of pressing that key several "
#~ "times in succession: Tab characters continue to be emitted until you "
#~ "release the key."
#~ msgstr ""
#~ "Если установлен этот параметр, нажатие и удерживание клавиши приводит к "
#~ "тому, что клавиша выдаёт код символа снова и снова (аналогично нажатию "
#~ "клавиши несколько раз)."

#~ msgid "&Enable keyboard repeat"
#~ msgstr "По&вторять ввод символа"
